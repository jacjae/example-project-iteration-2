<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->


## License

Copyright © 2020 German Aerospace Center (DLR)

This work is licensed under multiple licenses:
- The source code and the accompanying material is licensed under [MIT](LICENSES/MIT.txt).
- Insignificant files are licensed under [CC0-1.0](LICENSES/CC0-1.0.txt).

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
